import Basic.deck;
import Middle.Croupier;
import Middle.Player;

/**
 * Created by msala on 19-Mar-17.
 */
public class Game {
    public Game(int player_money) {
        _croupier = new Croupier();
        _player = new Player(player_money);
        _deck = new deck();
    }
    public Player _player;
    public Croupier _croupier;
    public deck _deck;
    public void setup_game(){
        _croupier.restart();
        _player.restart();
        _deck.generate_stack();
        _deck.shuffle_cards();
        _player.get_card(_deck.get_card());
        _deck.remove();
        _player.get_card(_deck.get_card());
        _deck.remove();
        _croupier.get_card(_deck.get_card());
        _deck.remove();
        _croupier.set_hidden_card(_deck.get_card());
        _deck.remove();
        _player.manage_money(0,10);         //Pay 10 to join game
    }
}

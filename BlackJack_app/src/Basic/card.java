package Basic;

/**
 * Created by msala on 19-Mar-17.
 */
public class card {
    public card(int value, char color, String symbol) {
        this._value = value;
        this._color = color;
        this._symbol = symbol;
    }
    private int _value;
    private char _color;
    private String _symbol;
    public int get_value(){
        return this._value;
    }
    public char get_color(){
        return this._color;
    }
    public String get_symbol(){ return this._symbol;}
}

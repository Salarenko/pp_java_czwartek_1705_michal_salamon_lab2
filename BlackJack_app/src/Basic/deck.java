package Basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by msala on 19-Mar-17.
 */
public class deck {

    List<card> deck = new ArrayList<card>();

    public void generate_stack(){
        char[] colors = {'♠','♥','♦','♣'};
        String[] symbols = {"2","3","4","5","6","7","8","9","10","J","K","Q","A"};
        int iterator =0;
        int value=0;
        for(int i=0; 13 > i; i++){
            for(int j=0; 4 >j; j++){
                if(i == 12)
                    value = 1;
                else if( i >= 9)
                    value = 10;
                else
                    value = Integer.valueOf(symbols[i]);
                deck.add(new card(value, colors[j], symbols[i]));
                iterator++;
            }
        }
        shuffle_cards();
    }
    public void shuffle_cards(){
        Collections.shuffle(this.deck);
    }
    public void remove(){
        deck.remove(0);
    }
    public card get_card(){
        card karta = deck.get(0);
        remove();
        return karta;
    }
    public boolean deck_empty(){
        return !(deck.size()>0);
    }
}

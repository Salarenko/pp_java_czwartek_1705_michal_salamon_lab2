import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import static java.lang.System.exit;

/**
 * Created by msala on 13-Mar-17.
 */
public class BlackJack_GUI {
    private JPanel panel1;
    private JButton hitButton;
    private JButton bet25Button;
    private JButton bet10Button;
    private JButton bet5Button;
    private JButton doubleButton;
    private JButton standButton;
    private JLabel playerCardsLabel;
    private JLabel moneyLabel;
    private JLabel betLabel;
    private JLabel croupierCardsLabel;
    private JButton newGameButton;
    private JButton exitButton;
    private JButton playButton;
    Game game = new Game(0);
    private int _bet=0;
    private boolean used_hit = false;
    private boolean used_double = false;
    private boolean used_stand = false;
    private String winner = "";
    private void reset(){
        used_hit = false;
        used_double = false;
        used_stand = false;
        winner = "Player";
        _bet = 10;
        croupierCardsLabel.setText(game._croupier.show_cards(false));
        playerCardsLabel.setText(game._player.show_cards());
        betLabel.setText(Integer.toString(_bet));
        moneyLabel.setText(Integer.toString(game._player.get_money()));
    }

    public BlackJack_GUI() {
        bet5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(game._player.has_enough_money(5) && !used_hit && !used_stand) {
                    game._player.manage_money(0, 5);
                    _bet += 5;
                    moneyLabel.setText(Integer.toString(game._player.get_money()));
                    betLabel.setText(Integer.toString(_bet));
                }
            }
        });
        bet10Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(game._player.has_enough_money(10) && !used_hit && !used_stand) {
                    game._player.manage_money(0, 10);
                    _bet += 10;
                    moneyLabel.setText(Integer.toString(game._player.get_money()));
                    betLabel.setText(Integer.toString(_bet));
                }
            }
        });
        bet25Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(game._player.has_enough_money(25) && !used_hit && !used_stand) {
                    game._player.manage_money(0, 25);
                    _bet += 25;
                    moneyLabel.setText(Integer.toString(game._player.get_money()));
                    betLabel.setText(Integer.toString(_bet));
                }
            }
        });
        hitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!game._deck.deck_empty() && !used_double && !used_stand ) {
                    game._player.get_card(game._deck.get_card());
                    playerCardsLabel.setText(game._player.show_cards());
                    used_hit = true;
                }
            }
        });
        doubleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!game._deck.deck_empty() && !used_hit && !used_stand && game._player.has_enough_money(_bet)) {
                    game._player.get_card(game._deck.get_card());
                    playerCardsLabel.setText(game._player.show_cards());
                    game._player.manage_money(0, _bet);
                    _bet *= 2;
                    betLabel.setText(Integer.toString(_bet));
                    used_hit = true;
                    used_double = true;
                }
            }
        });
        standButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                used_stand = true;
                if(game._player.get_score()>21){                            // Gracz ma wiecej niz 21
                    winner = "Croupier";
                }
                else {
                    while (game._croupier.think_of_taking_card())            // Krupier dobiera karty
                        game._croupier.get_card(game._deck.get_card());
                    croupierCardsLabel.setText(game._croupier.show_cards(true));       // Odsłania karty
                    if (game._player.get_score() == game._croupier.get_score(true)) {  // Remis
                        winner = "Draw";
                    }
                    else if(game._croupier.get_score(true) >21) {   // Krupier ma wiecej niz 21 a gracz mniej
                        winner = "Player";
                    }
                    else if (game._player.get_score() < game._croupier.get_score(true))
                            winner = "Croupier";
                    else{
                        winner = "Player";
                    }
                }
                switch(winner){
                    case "Player":
                        game._player.manage_money(_bet*2,0);
                        break;
                    case "Croupier":
                        break;
                    case "Draw":
                        break;
                }
                JOptionPane.showMessageDialog(null, "Winner: " + winner);
            }
        });
        newGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game = new Game(1000);
                game.setup_game();
                _bet =10;
                moneyLabel.setText(Integer.toString(game._player.get_money()));
                playerCardsLabel.setText(game._player.show_cards());
                croupierCardsLabel.setText(game._croupier.show_cards(false));
                betLabel.setText(Integer.toString(_bet));
                used_hit = false;
                used_double = false;
                used_stand = false;
            }
        });
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exit(0);
            }
        });
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!game._player.has_enough_money(10))
                    JOptionPane.showMessageDialog(null, "You have no more money :(");
                if (used_stand && game._player.has_enough_money(10)) {
                    game.setup_game();
                    reset();
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("Euler_52_GUI");
        frame.setContentPane(new BlackJack_GUI().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        BlackJack_GUI frames = new BlackJack_GUI();
        frame.setSize(400,400);
    }
}

package Middle;

import Basic.card;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

/**
 * Created by msala on 19-Mar-17.
 */
public class Player {
    private List<card> _player_cards = new ArrayList<card>();
    private int _money;

    public Player(int _money) {
        this._money = _money;
    }
    public int get_score(){
        int score = 0;
        boolean has_as = false;
        for(card karta : _player_cards){
            score += karta.get_value();
            if(karta.get_symbol() == "A")
                has_as = true;
        }
        if(abs(21 - score) <= abs(21 - (score+9)) &&  ((21 - (score+9)) >= 0))
            return score+9;
        return score;
    }
    public String show_cards(){
        String cards ="";
        for(card karta : _player_cards){
            cards += karta.get_symbol();
            cards += karta.get_color();
            cards += " ";
        }
        return cards;
    }
    public boolean has_enough_money(int money){
        if(_money>=money)
            return true;
        return false;
    }
    public void get_card(card card){
        _player_cards.add(card);
    }
    public void manage_money(int won, int lost){
        this._money += won;
        this._money -= lost;
    }
    public void restart(){
        _player_cards.clear();
    }
    public int get_money() {
        return _money;
    }
}

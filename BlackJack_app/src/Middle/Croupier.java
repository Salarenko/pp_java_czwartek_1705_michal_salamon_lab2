package Middle;

import Basic.card;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

/**
 * Created by msala on 19-Mar-17.
 */
public class Croupier {
    private List<card> _croupier_cards = new ArrayList<card>();
    private card _hidden_card;
    public String show_cards(boolean show_hidden){
        String cards ="";
        for(card karta : _croupier_cards){
            cards += karta.get_symbol() + karta.get_color() + " ";
        }
        if(show_hidden)
            cards += _hidden_card.get_symbol() + _hidden_card.get_color() + " ";
        else
            cards += "? ?";
        return cards;
    }
    public void get_card(card card){
        _croupier_cards.add(card);
    }
    public void set_hidden_card(card card){_hidden_card = card; }
    public int get_score(boolean count_hidden){
        int score = 0;
        boolean has_as = false;
        for(card karta : _croupier_cards){
            score += karta.get_value();
            if(karta.get_symbol() == "A")
                has_as = true;
        }
        if(count_hidden) {
            score += _hidden_card.get_value();
            if (_hidden_card.get_symbol() == "A")
                has_as = true;
        }
        if(abs(21 - score) <= abs(21 - (score+9)) &&  ((21 - (score+9)) >= 0))
            return score+9;
        return score;
    }
    public boolean think_of_taking_card(){
        if(get_score(false)>15)
            return false;
        return true;
    }
    public void restart(){
        _croupier_cards.clear();
    }
}
